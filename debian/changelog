ktp-approver (22.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.3).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 01 Mar 2023 11:58:01 +0100

ktp-approver (22.12.1-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.1).
  * Bump Standards-Version to 4.6.2, no change required.
  * Add Albert Astals Cid’s master key to upstream signing keys.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 06 Jan 2023 23:39:40 +0100

ktp-approver (22.12.0-2) unstable; urgency=medium

  * Upload with a correctly released changelog.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 10 Dec 2022 16:04:55 +0100

ktp-approver (22.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.08.3).
  * Update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.6.1, no change required.
  * New upstream release (22.11.90).
  * Update build-deps and deps with the info from cmake.
  * New upstream release (22.12.0).
  * Add lintian override for using the ${source:Upstream-Version} variable to
    keep the kde-telepathy-data in sync.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 10 Dec 2022 00:13:05 +0100

ktp-approver (21.12.3-1) unstable; urgency=medium

  * Added myself to the uploaders.
  * New upstream release (21.12.3).
  * Bump Standards-Version to 4.6.0, no change required.
  * Drop dbgsym migration rules, not needed anymore.
  * Drop kde-l10n migration rules, not needed anymore.
  * Build with hardening=+all build hardening flag.
  * Refresh upstream metadata.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 04 Apr 2022 21:39:45 +0200

ktp-approver (21.08.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (21.08.0).

 -- Norbert Preining <norbert@preining.info>  Mon, 16 Aug 2021 16:29:57 +0900

ktp-approver (21.04.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (21.04.0).
  * Borrow minimal upstream signing key from k3b.
  * Update watch file for signatures.
  * Drop Kubuntu from maintainer name.
  * Removed Maximiliano Curia, Mark Purcell from the uploaders, thanks
    for your work on the package!
  * Added myself to the uploaders.

 -- Norbert Preining <norbert@preining.info>  Thu, 06 May 2021 10:31:17 +0900

ktp-approver (20.08.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file to the new release-service location.
  * Update the build dependencies according to the upstream build system:
    - bump libtelepathy-qt5-dev to 0.9.8
    - explicitly add gettext
  * Switch from dhmk to the dh sequencer:
    - invoke the dh sequencer using the kf5 addon
    - call the right debhelper command instead of $(overridden_command)
    - manually force the generation of the substvars for the kde-l10n breaks
  * Bump the debhelper compatibility to 13:
    - switch the debhelper build dependency to debhelper-compat 13
    - remove debian/compat
  * Add Rules-Requires-Root: no.
  * Bump Standards-Version to 4.5.0, no changes required.
  * Switch Vcs-* fields to salsa.debian.org.

 -- Pino Toscano <pino@debian.org>  Thu, 13 Aug 2020 22:01:30 +0200

ktp-approver (17.08.3-2) unstable; urgency=medium

  * Team upload.
  * Fix typo in variable in debian/rules.

 -- Pino Toscano <pino@debian.org>  Sat, 09 Dec 2017 16:05:25 +0100

ktp-approver (17.08.3-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * New upstream release.
  * Bump Standards-Version to 4.1.2, no changes required.
  * Simplify watch file, and switch it to https.
  * Stop forcing debian/tmp as local installation directory, installing
    everything to the final location
    - drop kde-telepathy-approver.install, no more useful now
    - no more need to invoke dh_install --fail-missing
  * Bump telepathy-related build dependencies:
    - libtelepathy-logger-qt-dev to >= 17.08
    - libtelepathy-qt5-dev to >= 0.9.7
  * Fix override for dh_auto_configure, so it is actually used.
  * Adjust l10npkgs_firstversion_ok to the version where kde-l10n will
    drop translations.

 -- Pino Toscano <pino@debian.org>  Sat, 09 Dec 2017 08:40:29 +0100

ktp-approver (17.08.1-1) experimental; urgency=medium

  * New upstream release (17.08.1).
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Mon, 11 Sep 2017 19:59:02 +0200

ktp-approver (16.04.2-1) experimental; urgency=medium

  * New upstream release.

 -- Maximiliano Curia <maxy@debian.org>  Thu, 23 Jun 2016 09:44:12 +0200

ktp-approver (16.04.1-1) experimental; urgency=medium

  [ Automatic packaging ]
  * Bump Standards-Version to 3.9.8
  * Update build-deps and deps with the info from cmake
  * Add a .gitattributes file to use dpkg-mergechangelogs

  [ Diane Trout ]
  * Fix typo in debug line

  [ Maximiliano Curia ]
  * Replace the "Historical name" ddeb-migration by its "Modern, clearer" replacement dbgsym-migration.
  * Add upstream metadata (DEP-12)
  * debian/control: Update Vcs-Browser and Vcs-Git fields
  * Add a basic gbp conf file
  * Update vcs uris to point to the ones in applications

 -- Maximiliano Curia <maxy@debian.org>  Fri, 27 May 2016 11:11:12 +0200

ktp-approver (15.12.1-1) experimental; urgency=medium

  * Update watch file.
  * New upstream release (15.12.0).
  * New upstream release (15.12.1).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 01 Feb 2016 10:24:38 +0100

ktp-approver (15.08.3-1) unstable; urgency=medium

  * New upstream release.

 -- Diane Trout <diane@ghic.org>  Wed, 02 Dec 2015 21:29:37 -0800

ktp-approver (15.08.2-1) unstable; urgency=medium

  * Apply homepage update patch from Matthew Cope
  * Update watch file for KTps move to Applications
  * New upstream release.
  * Update build dependencies to include:
    - qtbase5-dev, extra-cmake-modules, libsignon-qt5-dev,
      libtelepathy-qt5-dev, libtelepathy-logger-qt-dev
      and several libkf5 libraries.
  * Update debian/copyright.
    - Add myself top debian/* copyright.
    - Add LGPL-2+ block for src/ktp_approver_debug.*
    - remove po/* block.
  * Adjust paths in kde-telepathy-approver for KF5 locations.
  * Use --fail-missing in rules to make it easier for me to find
    whats missing.
  * Use dhmk from pkg-kde-tools to build ktp.
    - Change maintainer to debian-qt-kde
    - Update debian/rules
    - Update plugins path to qt5/plugins.
  * Add KDE's unstable download url to watch file

 -- Diane Trout <diane@ghic.org>  Sat, 07 Nov 2015 10:35:45 -0800

ktp-approver (0.9.0-1) unstable; urgency=medium

  * New upstream release.
  * Change kde-telepathy-data depends to source:Upstream-Version
  * Update Standards-Version to 3.9.6. No changes needed.
  * Update debian/copyright.
  * Update debian/watch file to use http site.

 -- Diane Trout <diane@ghic.org>  Sat, 25 Apr 2015 20:17:10 -0700

ktp-approver (0.8.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump kde-telepathy-data dependency to >= 0.8.1
  * Update Standards-Version to 3.9.5. No changes needed.
  * Update debian/copyright.

 -- Diane Trout <diane@ghic.org>  Fri, 16 May 2014 20:45:11 -0700

ktp-approver (0.7.0-1) unstable; urgency=low

  * New upstream release.
  * servicetypes/ktp-approver.service renamed to ktp-approver.desktop
  * Bump kde-telepath-data dependency to >= 0.7

 -- Diane Trout <diane@ghic.org>  Wed, 27 Nov 2013 18:07:05 -0800

ktp-approver (0.6.3-1) unstable; urgency=low

  [ Diane Trout ]
  * Imported Upstream version 0.6.2
  * Set Uploaders to Diane Trout and Michał Zając
  * Install ktp-approver.service
  * Update Standards-Version to 3.9.4. No changes needed.
  * Update watch file as KDE-Telepathy is now in KDE stable.

  [ Mark Purcell ]
  * Imported Upstream version 0.6.3
  * Reinstate debian/ to origin/master
  * Add myself to Uploaders

 -- Mark Purcell <msp@debian.org>  Sun, 11 Aug 2013 10:12:02 +1000

ktp-approver (0.4.0-1) unstable; urgency=low

  * Initial release.

 -- George Kiagiadakis <kiagiadakis.george@gmail.com>  Mon, 18 Jun 2012 22:13:17 +0300
